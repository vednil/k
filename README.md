# Установка Ubuntu Server

[[_TOC_]]


Установка операционной системы производится в режиме UEFI с отключенным Secure Boot.
- Если используется BIOS, то нужно создать таблицу разделов GPT на дисках в _Rescue Mode_ с помощью _parted_.

Ссылки для mini образов:
- [Ubuntu Server 18.04](http://archive.ubuntu.com/ubuntu/dists/bionic-updates/main/installer-amd64/current/images/netboot/mini.iso)
- [Ubuntu Server 18.04 HWE](http://archive.ubuntu.com/ubuntu/dists/bionic-updates/main/installer-amd64/current/images/hwe-netboot/mini.iso)
- [Ubuntu Server 20.04](http://archive.ubuntu.com/ubuntu/dists/focal-updates/main/installer-amd64/current/legacy-images/netboot/mini.iso)


## Установка операционной системы

1. Выбираем _Install_ при запуске с помощью образа.
2. Выбираем настройки клавиатуры:
   - язык _English_;
   - страна _Other -> Europe -> Russian Federation_;
   - локаль _United States - en_US.UTF-8_;
   - обнаружение раскладки _No_;
   - страна клавиатуры _Russian_;
   - раскладка _Russian_;
   - переключение раскладок _No toggling_.
3. Настраиваем сеть:
   - выбираем сетевой интерфейс с доступом в интернет - система попытается настроить сеть через DHCP;
   - при отсутствии возможности автоматической настройки будет предложено выбрать метод настройки сети, выбираем _Configure network manually_ - задаем IP адрес, маску, шлюз и _1.1.1.1 8.8.8.8_ в качестве DNS;
   - указываем имя хоста;
   - имя домена оставляем пустым.
4. Выбираем страну зеркала _Russian Federation_.
5. Заполняем настройки прокси при необходимости.
6. Указываем имя пользователя и пароль.
7. Выбор часового пояса:
   - отвечаем _No_ при вопросе о корректности предложенного часового пояса;
   - выбираем _Moscow+00 - Moscow_.
8. Выбираем метод разбивки дисков _Manual_.
9. Создаем разделы /boot/efi и /boot объемом 250 МБ и 1 ГБ соответственно на каждом диске, который планируется добавить в массив RAID для установки операционной системы.
   - В режиме BIOS создаем bios_grub объемом 1 МБ вместо раздела /boot/efi.
10. Создаем 2 программных RAID из раздела /boot и свободного пространства на дисках с этими разделами:
   - выбираем _Configure software RAID_;
   - подтверждаем запись изменений - _Yes_;
   - действие _Create MD device_;
   - тип _RAID1_;
   - количество активных устройств - 2, а запасных - 0;
   - далее выбираем 2 одинаковых раздела дисков;
   - подтверждаем запись изменений и нажимаем _Finish_;
   - используем RAID массив 0 как /boot.
11. Настраиваем LVM:
   - выбираем _Configure the Logical Volume Manager_;
   - подтверждаем запись изменений - _Yes_;
   - создаем группу LVM - _Create volume group_;
   - имя группы указываем system;
   - выбираем созданный из свободного пространства массив RAID;
   - далее создаем логический том - _Create logical volume_;
   - выбираем ранее созданную группу LVM;
   - имя тома - root;
   - объем - 100 ГБ;
   - завершаем действия с LVM, нажав _Finish_;
   - используем созданный логический том LVM с именем root как /.
12. Записываем сделанные изменения _Finish partitioning and write changes to disk_.
13. Выбираем автоматическую установку обновлений безопасности - _Install security updates automatically_.
14. Устанавливаем _OpenSSH server_ и _Basic Ubuntu server_.
15. Если системные часы не установлены в соответствии с UTC, то выбираем _No_.
16. Установка завершена, нажимаем _Continue_.


## Настройка сервера

### Отключение swap

Нам на сервере не требуется файл подкачки, поэтому отключаем его:
```
sudo sed -i '/ swap / s/^/#/' /etc/fstab && \
sudo swapoff -a && \
sudo rm /swapfile
```

### Настройка сети

1. Вносим изменения в GRUB:
```
sudo sed -i 's/GRUB_CMDLINE_LINUX=""/GRUB_CMDLINE_LINUX="netcfg\/do_not_use_netplan=true"/g' /etc/default/grub && \
sudo update-grub
```
2. Удаляем netplan:
```
sudo apt-get purge netplan.io
```
3. Удаляем конфигурационные файлы:
```
sudo rm -rf /etc/netplan
```
4. Устанавливаем пакет ifupdown:
```
sudo apt-get install ifupdown net-tools
```
5. Указываем настройки сети в файле /etc/network/interfaces. Можно использовать пример ниже, заменив имя интерфейса eth0 на правильный и указав соответствующие IP адрес, маску и шлюз. Пример:
```
# ifupdown has been replaced by netplan(5) on this system.  See
# /etc/netplan for current configuration.
# To re-enable ifupdown on this system, you can run:
#    sudo apt install ifupdown

# The loopback network interface
auto lo
iface lo inet loopback

# The primary network interface
auto eth0
iface eth0 inet static
  address 192.168.0.10
  netmask 255.255.255.0
  gateway 192.168.0.1
```
6. Настраиваем DNS:
```
sudo sed -i 's/#DNS=/DNS=1.1.1.1 8.8.8.8/g' /etc/systemd/resolved.conf && \
sudo systemctl restart systemd-resolved
```
7. Перезагружаем сервер.

### Настройка SSH

1. Добавляем публичную часть SSH ключа для своего пользователя:
```
export SSH_PUBLIC_KEY="here must be your public key"
```
```
mkdir .ssh && \
chmod 700 .ssh && \
echo "$SSH_PUBLIC_KEY" > .ssh/authorized_keys && \
chmod 644 .ssh/authorized_keys
```
2. Отключаем доступ по паролю:
```
sudo sed -i 's/#PasswordAuthentication yes/PasswordAuthentication no/g' /etc/ssh/sshd_config
```
3. Выполняем перезапуск ssh:
```
sudo systemctl restart ssh
```
